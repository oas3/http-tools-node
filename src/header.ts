export function getFirstHeaderValue(
    headers: NodeJS.Dict<string | string[]>,
    headerName: string,
) {
    for (const headerValue of readAllHeaderValues(headers, headerName)) {
        return headerValue;
    }

    return;
}

export function getAllHeaderValues(
    headers: NodeJS.Dict<string | string[]>,
    headerName: string,
) {
    return [...readAllHeaderValues(headers, headerName)];
}

function* readAllHeaderValues(
    headers: NodeJS.Dict<string | string[]>,
    headerName: string,
) {
    const rawValue = headers[headerName];
    if (rawValue == null) return;

    if (Array.isArray(rawValue)) {
        for (const headerValue of rawValue) {
            yield* parseHeaderValues(headerValue);
        }
    }
    else {
        yield* parseHeaderValues(rawValue);
    }

}

function parseHeaderValues(headerValue: string) {
    return headerValue.split(",").
        map(value => value.trim());
}

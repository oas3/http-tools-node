import * as http from "http";
import { getFirstHeaderValue } from "./header.js";

export function getSourceIp(incomingMessage: http.IncomingMessage) {
    // TODO: Support Forwarded header

    {
        const forwarderFor = getFirstHeaderValue(
            incomingMessage.headers,
            "x-forwarded-for",
        );

        if (forwarderFor != null) {
            return forwarderFor;
        }
    }

    {
        const address = incomingMessage.socket.address();
        if ("address" in address) {
            return address.address;
        }
    }

    return;
}
